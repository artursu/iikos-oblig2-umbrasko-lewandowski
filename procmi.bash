#!/bin/bash

#echo "Eksempel på datoformat: $(date +%Y%m%d-%H:%M:%S)"
#cat /proc/22316/status | grep VmSize | tr -d -c [0-9]

for i in "$@";
do
  size=$(cat "/proc/$i/status" | grep VmSize | tr -d -c "0-9")
  data=$(cat "/proc/$i/status" | grep VmData | tr -d -c "0-9")
  stk=$(cat "/proc/$i/status" | grep VmStk | tr -d -c "0-9")
  exe=$(cat "/proc/$i/status" | grep VmExe | tr -d -c "0-9")
  lib=$(cat "/proc/$i/status" | grep VmLib | tr -d -c "0-9")
  rss=$(cat "/proc/$i/status" | grep VmRSS | tr -d -c "0-9")
  pte=$(cat "/proc/$i/status" | grep VmPTE | tr -d -c "0-9")

  privateVirtual="$(echo "$data+$stk+$exe" | bc)"

  filename="$i-$(date +%Y%m%d-%H:%M:%S).meminfo"

  touch "$filename"

  echo "********Memory info of process $i********" >> "$filename"
  echo "Total usage of virtual memory (VmSize): $size kB" >> "$filename"
  echo "Amount of private virtual memory (VmData + VmStk + VmExe): $privateVirtual kB" >> "$filename"
  echo "Amount of shared virtual memory (VmLib): $lib kB" >> "$filename"
  echo "Total usage of physical memory (VmRSS): $rss kB" >> "$filename"
  echo "Physical memory used for page table (VmPTE): $pte kB" >> "$filename"

done


#VmSize
#VmData
#VmStk
#VmExe
#VmLib
#VmRSS
#VmPTE
