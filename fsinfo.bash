#!/bin/bash
cd "$1" || return
totalAmount=$(find . -type f | wc -l)
totalSize=$(du . -a | sort -n -r | head -n 1 | tr -d -c '0-9')

averageSize=$(echo -e "scale=4\n$totalSize/$totalAmount" | bc)

biggestFile=$(find . -type f -print0 | du -b --files0-from=- | sort -n -r | head -n 1)


echo "Total amount of files: $totalAmount"
echo "Total size: $totalSize kB"
echo "Average size: $averageSize kB"
echo "Biggest file is: $biggestFile"

allHards=$(find . -type f -links +1)
biggest=0
output=""
for i in $allHards;
do

if [[ $(find . -samefile "$i" | wc -l) -gt biggest ]];
then

  biggest=$(find . -samefile "$i" | wc -l)
  output=$i

fi

done
echo "Most amount of hard links: $output with $biggest hard links"
