#!/bin/bash
# no arguments, display a menu with option for info
# about processes

clear
#name="myprocinfo.bash"
while [ "$svar" != "9" ]
do
 echo ""
 echo "  1 - Hvem er jeg?"
 echo "  2 - Hvor lenge er det siden siste boot"
 echo "  3 - Hva er gjennomsnittlig load siste minutt?"
 echo "  4 - Hvor mange prosesser og trader finnes?"
 echo "  5 - Hvor mange context switch'er fant sted siste sekund?"
 echo "  6 - Hvor mange interrupts fant sted siste sekund?"
 echo "  9 - Avslutt dette scriptet"
 echo ""
 echo -n "Velg en funksjon: "
 read -r svar
 echo ""
 case $svar in
  1)clear
    echo "Jeg er $(whoami)"
    echo "$0"
    read -r
    clear
    ;;
  2)clear
    echo "Uptime is:"
    uptime | cut -c15-18
    read -r
    clear
    ;;
  3)clear
    uptime | cut -c31-61
    read -r
    clear
    ;;
  4)clear
    echo "Amount of processes and threads"
    ps -eLf | wc -l
    read -r
    clear
    ;;
  5)clear
  echo "Gathering amount of switches"
  contextbefore=$(cat /proc/stat | grep ctxt | cut -c6-20)
  sleep 1
  contextafter=$(cat /proc/stat | grep ctxt | cut -c6-20)
  echo "$(echo "$contextafter-$contextbefore" | bc)"
  read -r
  clear
  ;;
  6)clear
    echo "Gathering amount of interrupts"
    contextbefore=$(cat /proc/stat | grep intr | cut -c6-13)
    sleep 1
    contextafter=$(cat /proc/stat | grep intr | cut -c6-13)
    echo "$(echo "$contextafter-$contextbefore" | bc)"
    read -r
    clear
    ;;
  9)echo Scriptet avsluttet
    exit
    ;;
  *)echo Ugyldig valg
    read -r
    clear
    ;;
 esac
done
