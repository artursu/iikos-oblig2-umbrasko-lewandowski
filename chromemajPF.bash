#!/bin/bash

name=$(pgrep chrome)
for i in $name; do
  faults=$("ps --no-headers -o maj_flt $i")
  if [[ "$faults" -gt 1000 ]];
  then
    echo "Chrome $i has created $faults major page faults! (more than 1000!)"
  else
    echo "Chrome $i has created $faults major page faults!"
  fi
done
