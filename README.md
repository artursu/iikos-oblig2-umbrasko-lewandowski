# Obligatorisk oppgave 2 - Bash pipelines og scripting

Denne oppgaven best�r av de f�lgende laboppgavene fra kompendiet:

* 7.5.a (Prosesser og tr�der)
* 8.6.c (Page faults)
* 8.6.d (En prosess sin bruk av virtuelt og fysisk minne)
* 9.4.a (Informasjon om deler av filsystemet)

## Gruppemedlemmer

* Artūrs Umbraško
* Kacper Lewandowski

## Comments

* Pipeline still returns some warnings, but we are not sure how to fix that
